# Publications and Research Projects
__Louis Huebser__  
Research Associate at Laboratory for Machine Tools and Production Engineering (WZL) of RWTH Aachen University, Chair of Production Metrology and Quality Management
## Publications
1. __Concept for an augmented intelligence-based quality assurance of assembly tasks in global value networks__  
8th CIRP Conference of Assembly Technology and Systems  
https://www.sciencedirect.com/science/article/pii/S2212827120314840  
2. __Masing Handbuch Qualitätsmanagement__  
Carl Hanser Verlag  
https://www.hanser-elibrary.com/doi/book/10.3139/9783446466210  
Chapters:
    - 'Statistische Ansätze und Maschinelles Lernen'
    - 'Anwendungsbeispiel des Maschinellen Lernens in der Produktion'
    - 'Einführung von Predictive Quality aus unternehmerischer Sicht'
3. __Korrelationsbasierte Erkennung von Montagereihenfolgen mittels 6 DoF-Zeitreihendaten zur Prozessdiagrammerstellung__  
Gesellschaft für Qualitätswissenschaft, Tagungsband 2020  
(in press)  
source code: https://gitlab.com/LHuebser/cbag  
4. __Mit Predictive Quality in die Zukunft sehen__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb, Band 115, Heft 10  
https://www.degruyter.com/document/doi/10.1515/zwf-2020-1151015/html    
5. __Besseres Verständnis : Analyse von Produktionsprozessen mit Predictive Quality-Ansätzen__  
Qualität und Zuverlässigkeit : QZ  
https://www.qz-online.de/a/fachartikel/besseres-verstaendnis-210423  
6. __Industrie 4.0 & Digitalisierung in der manuellen Montage am Beispiel eines Nutzfahrzeugherstellers__  
Qualität und Zuverlässigkeit : QZ  
https://www.qz-online.de/a/fachartikel/digitalisierung-in-der-manuellen-montage-339859  
7. __Conception of Generative Assembly Planning in the Highly Iterative Product Development__  
7.te WGP-Jahreskongress Aachen, 5.-6. Oktober 2017  
https://d-nb.info/1155019458/34#page=323  
8. __NFDI4Ing - the National Research Data Infrastructure for Engineering Sciences__  
https://mediatum.ub.tum.de/1575816
9. __Augmented Intelligence – Mensch trifft Künstliche Intelligenz__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb  
https://www.degruyter.com/document/doi/10.1515/zwf-2021-0104/html  
10. __Prädiktive Qualität in der Prozesslenkung: Neuronales Netz als SPC 4.0__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb  
https://www.degruyter.com/document/doi/10.1515/zwf-2021-0134/pdf  
11. __Potentiale von Neuronalen Netzen gegenüber SPC zur Fehlervermeidung in der Prozesssteuerung__  
Gesellschaft für Qualitätswissenschaft, Tagungsband 2021  
12. __Digital Vehicle Protocol based on Distributed Ledger Technology in Production__  
Procedia CIRP CMS 2022 - 55th CIRP Conference on Manufacturing Systems  
13. __Datenbasierte Entscheidungsunterstützung__  
_Wie Qualität durch die Nutzung geeigneter Analysemethoden optimiert wird_
Qualität und Zuverlässigkeit: QZ, Jahrgang 67 (2022)
14.	__Benchmarking Control Charts and Machine Learning Methods for Fault Prediction in Manufacturing__  
Wissenschaftliche Gesellschaft für Produktionstechnik WGP 2022 (in press)
15.	__Fehlerdatenaufnahme in der manuellen Montage: Informationsbedarfsanalyse für die Fehleranalyse und -abstellung im Fehlermanagement__  
Gesellschaft für Qualitätswissenschaft, Tagungsband 2022 (in press)
16.	__Integration von Predictive Quality im Unternehmen__  
Qualität und Zuverlässigkeit QZ 2022 (in press)
17.	__Hybrid Intelligence: Augmenting employees’ decision-making with AI-based applications__  
In Handbook of Human-Machine Systems, IEEE 2021
18.	__Improving shopfloor-near Production Management through data-driven Insights__  
In Internet of Production - Fundamentals, Applications and Proceedings, Springer 2022 (in press )
19.	__Predictive Quality in der Produktion__  
Qualität und Zuverlässigkeit QZ 2022
20.	__Datengetriebenes Fehlermanagement in der Produktion__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb
21. __Reducing Wastage In Manufacturing Through Digitalization: An Adaptive Solution Approach For Process Efficiency__   
CONFERENCE ON PRODUCTION SYSTEMS AND LOGISTICS CPSL 2023
22. __Handlungsstufenplan für Digitalisierungsprojekte in der Produktion__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb
23. __Humanzentrierter Gestaltungsprozess eines intelligenten Assistenzsystems in der manuellen Montage__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb (in press)
24. __Generalized Statistical Process Control via 1D-ResNet Pretraining__  
CIRP ICME '23 
25. __Machine Learning Applied to Industrial Assembly Lines: A Bibliometric Study__  
Industrial Engineering and Operations Management
26. __Manufacturing Data Analytics: Wege zur Steigerung der Wettbewerbsfähigkeit__  
Zeitschrift für wirtschaftlichen Fabrikbetrieb

### In-Progress
- _Heuristic Cloud Point Matching with Large Displacements_
- _Transformer-based Object Localization and 6 DoF Pose Estimation_

## Research Projects
1. __AuQuA__  
Augmented Intelligence based Quality Assurance of Assembly Tasks in Global Value Networks  
https://wzl-lotus3.wzl.rwth-aachen.de/cms/auqua/de/f80c442d50e82d0fc1257e6f00315f53.html  
2. __AIXPERIMENTATIONLAB__  
Augmented Intelligence Experimentation Laboratory  
http://www.aixperimentationlab.wzl.rwth-aachen.de/de/f80c442d50e82d0fc1257e6f00315f53.html
3. __Exzellenzcluster Internet of Production__
Digitale und vernetzte Produktion.  
Workstream-Lead "Short-Term Production Management"
